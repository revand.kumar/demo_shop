Feature: DemoWebShop Application

  Scenario: DemoWebShop Login Module
    Given open the website"https://demowebshop.tricentis.com/"
    When click Login button
    And Enter the email "manzmehadi1@gmail.com" in Username
    And Enter the password "Mehek@110" in Password
    And Click the login button

  Scenario: DemoWebShop Books Module
    Then select Books in categories
    And Sort the books in High to Low
    And Add Two product into Cart

  Scenario: DemoWebShop Electronics Module
    Then select Electronics in categories
    And Select cellphones
    And Add  product into Cart
    And Display Count of the items added to the cart

  Scenario: DemoWebShop Gift cards Module
    Then select Giftcards in categories
    And Display Four per page and Select the GiftCard
    And Capture the Name and Price of the Gift card

  Scenario: DemoWebShop Logout Module
    And Logout from the Application
    And Capture the LoginProfile page
