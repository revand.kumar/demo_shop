package com.testDemo;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class demoShop_testng {
	WebDriver driver = null;


	@Test(priority = 1)
	public void Login() {
		driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		driver.findElement(By.xpath("//a[text()='Log in']")).click();
		driver.findElement(By.id("Email")).sendKeys("manzmehadi1@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Mehek@110");
		driver.findElement(By.xpath("//input[@value=\"Log in\"]")).click();

	}

	@Test(priority = 2)
	public void Books() throws InterruptedException {
		driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Books')]")).click();
		WebElement element = driver.findElement(By.id("products-orderby"));
		Select s = new Select(element);
		s.selectByVisibleText("Price: High to Low");
		driver.findElement(By.xpath("(//input[@type=\"button\"])[3]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//input[@type=\"button\"])[5]")).click();
		Thread.sleep(5000);

	}

	@Test(priority = 3)
	public void Electronic() throws InterruptedException {
		driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Electronics')]")).click();
		driver.findElement(By.xpath("//h2[@class='title']//a[contains(text(),' Cell phones')]")).click();
		driver.findElement(By.xpath("(//input[@type=\"button\"])[3]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//input[@type=\"button\"])[4]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[text()=\"Shopping cart\"]")).click();
	}

	@Test(priority = 4)
	public void Giftcard() throws InterruptedException, IOException {
		driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Gift Cards')]")).click();
		WebElement element1 = driver.findElement(By.id("products-pagesize"));
		Select s1 = new Select(element1);
		s1.selectByVisibleText("4");
		driver.findElement(By.xpath("//h2[@class='product-title']//a[contains(text(),'$5 Virtual Gift Card')]"))
				.click();
		TakesScreenshot scr = (TakesScreenshot) driver;
		File f = scr.getScreenshotAs(OutputType.FILE);
		File dest = new File("/home/revand/ECLIPSE/Demoshop_test/target/File/screenshot/screenshot.png");
		FileUtils.copyFile(f, dest);

	}

	@Test(priority = 5)
	public void Logout() throws InterruptedException, IOException {
		driver.findElement(By.xpath("//div[@class='header-links']//a[contains(text(),'Log out')]")).click();
		Thread.sleep(5000);
		TakesScreenshot scr = (TakesScreenshot) driver;
		File f = scr.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(f, new File("/home/revand/ECLIPSE/Demoshop_test/target/File/screenshot/screenshot1.png"));
		Thread.sleep(5000);
		driver.quit();
	}

}