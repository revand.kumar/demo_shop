package com.demoshop;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class demoShop_cucumber {
	static WebDriver driver = null;

	@Given("open the website\"https:\\/\\/demowebshop.tricentis.com\\/\"")
	public void open_the_website_https_demowebshop_tricentis_com() {
		driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
	}

	@When("click Login button")
	public void click_login_button() {
		driver.findElement(By.xpath("//a[text()='Log in']")).click();
	}

	@When("Enter the email {string} in Username")
	public void enter_the_email_in_username(String string) {
		driver.findElement(By.id("Email")).sendKeys(string);
	}

	@When("Enter the password {string} in Password")
	public void enter_the_password_in_password(String string) {
		driver.findElement(By.id("Password")).sendKeys(string);
	}

	@When("Click the login button")
	public void click_the_login_button() {
		driver.findElement(By.xpath("//input[@value=\"Log in\"]")).click();
	}

	@Then("select Books in categories")
	public void select_books_in_categories() {
		driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Books')]")).click();

	}

	@Then("Sort the books in High to Low")
	public void sort_the_books_in_high_to_low() {
		WebElement element = driver.findElement(By.id("products-orderby"));
		Select s = new Select(element);
		s.selectByVisibleText("Price: High to Low");

	}

	@Then("Add Two product into Cart")
	public void add_two_product_into_cart() throws InterruptedException {
		driver.findElement(By.xpath("(//input[@type=\"button\"])[3]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//input[@type=\"button\"])[5]")).click();
		Thread.sleep(5000);

	}

	@Then("select Electronics in categories")
	public void select_electronics_in_categories() {
		driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Electronics')]")).click();
	}

	@Then("Select cellphones")
	public void select_cellphones() {
		driver.findElement(By.xpath("//h2[@class='title']//a[contains(text(),' Cell phones')]")).click();
	}

	@Then("Add  product into Cart")
	public void add_product_into_cart() throws InterruptedException {
		driver.findElement(By.xpath("(//input[@type=\"button\"])[3]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("(//input[@type=\"button\"])[4]")).click();
		Thread.sleep(5000);

	}

	@Then("Display Count of the items added to the cart")
	public void display_count_of_the_items_added_to_the_cart() throws InterruptedException {
		driver.findElement(By.xpath("//span[text()=\"Shopping cart\"]")).click();

	}

	@Then("select Giftcards in categories")
	public void select_giftcards_in_categories() {
		driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Gift Cards')]")).click();

	}

	@Then("Display Four per page and Select the GiftCard")
	public void display_four_per_page_and_select_the_gift_card() {
		WebElement element1 = driver.findElement(By.id("products-pagesize"));
		Select s1 = new Select(element1);
		s1.selectByVisibleText("4");
	}

	@Then("Capture the Name and Price of the Gift card")
	public void capture_the_name_and_price_of_the_gift_card() throws IOException {
		driver.findElement(By.xpath("//h2[@class='product-title']//a[contains(text(),'$5 Virtual Gift Card')]"))
				.click();
		TakesScreenshot scr = (TakesScreenshot) driver;
		File f = scr.getScreenshotAs(OutputType.FILE);
		File dest = new File("/home/revand/ECLIPSE/Demoshop_test/target/File/screenshot/screenshot.png");
		FileUtils.copyFile(f, dest);
	}

	@Given("Logout from the Application")
	public void logout_from_the_application() throws InterruptedException {
		driver.findElement(By.xpath("//div[@class='header-links']//a[contains(text(),'Log out')]")).click();
		Thread.sleep(5000);
	}

	@Given("Capture the LoginProfile page")
	public void capture_the_login_profile_page() throws IOException, InterruptedException {
		TakesScreenshot scr = (TakesScreenshot) driver;
		File f = scr.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(f, new File("/home/revand/ECLIPSE/Demoshop_test/target/File/screenshot/screenshot1.png"));
		Thread.sleep(5000);
		driver.quit();
	}

}
