package com.demoshop;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "/home/revand/ECLIPSE/Demoshop_test/src/test/resources/featurefile",
		glue = {"com.demoshop"},
		plugin = {"pretty", "html:target/cucumber-report/report2.html"},
		dryRun = true,
		monochrome = true
		
		
		)
public class Runnerclass {

}
